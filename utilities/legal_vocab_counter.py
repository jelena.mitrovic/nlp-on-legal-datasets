"""

This script takes legal dictionary (words and definitions) and produces an updated dictionary
with number of judgements (majority, and dissent) using these legal words

"""


import pandas as pd


legal_dictionary = pd.read_csv('../data/legal_vocabulary/legal_dictionary.tsv', sep='\t')


legal_dictionary_counts = pd.DataFrame(columns=['Word', 'Definition', 'Majority_Count', 'Dissent_Count', 'Total_Count'])

#read the classification data
majority_decisions_df = pd.read_csv('../data/classification/judgement_type_majority.tsv', sep='\t')
dissent_decisions_df = pd.read_csv('../data/classification/judgement_type_dissent.tsv', sep='\t')

#lists of decisions
majority_decisions = majority_decisions_df['sentence'].tolist()
dissent_decisions = dissent_decisions_df['sentence'].tolist()
print(len(dissent_decisions))
print(len(majority_decisions))
# exit(0)

#takes a word
#returns: orrucrences of the word in majority and dissent decisions, and count of majority and dissent dicisions using this word
def count_decisions(word):
	return {
	"occurences_in_majority" :sum([ len(majority_decision.lower().split(word.lower()))-1 for majority_decision in majority_decisions]),
	"occurences_in_dissent" : sum([ len(dissent_decision.lower().split(word.lower()))-1 for dissent_decision in dissent_decisions]), 
	"majority_decisions" :sum([1 for majority_decision in majority_decisions if word.lower() in majority_decision.lower()]),
	"dissent_decisions" :sum([1 for dissent_decision in dissent_decisions if word.lower() in dissent_decision.lower()])
	}

for i in range(len(legal_dictionary)):
	word, definition = legal_dictionary['Word'][i], legal_dictionary['Definition'][i]
	stats = count_decisions(word)
	legal_dictionary_counts.loc[i] = [word] + [definition] + [stats["majority_decisions"]] + [stats["dissent_decisions"]] + [stats["majority_decisions"]+stats["dissent_decisions"]]


legal_dictionary_counts.to_csv('../data/legal_vocabulary/legal_dictionary.tsv', sep='\t', index=False)


