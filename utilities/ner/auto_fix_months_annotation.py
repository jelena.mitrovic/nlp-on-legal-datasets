#This script takes tagged data by nltk/spacy as input, as performed by notebooks 
# ../jupyter_notebooks/Label_NER_with_NLTK.ipynb and ../jupyter_notebooks/Label_NER_with_Spacy.ipynb

#Output: it shows the list of DATE type named entities detected for months in the input dataset that are not 
#marked correctly, and updates their label with DATE label, and writes final output in a separate file


input_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually.csv"
with open(input_filename, 'r') as f:
	content = f.readlines()

print(len(content))
out_content=[]
lines = 0
month_count=0

indexes=[]

MONTH = "December" #repeat  for the months that have problem
for x in range(0,len(content)):
	if ","+str(MONTH)+",O" in content[x]: 
		if content[x+1].split(",")[1].isnumeric() and content[x+3].split(",")[1].isnumeric(): #month day , year
			month_count=month_count+1
			#add these to update ner tag in the content to DATE
			indexes.append(x)
			indexes.append(x+1)
			indexes.append(x+3)
			# print(content[x].strip())
			# print(content[x+1].strip())
			# print(content[x+3].strip())
			# print("\n\n")
		elif content[x+1].split(",")[1].isnumeric(): #month year
			month_count=month_count+1
			# print(content[x].strip())
			# print(content[x+1].strip())
			# print("\n\n")
			indexes.append(x)
			indexes.append(x+1)
		elif (",\",\"," in content[x+1] or ",of," in content[x+1]) and content[x+2].split(",")[1].isnumeric(): #month , year // month of year
			month_count=month_count+1
			indexes.append(x)
			indexes.append(x+2)
			# print(x)	
			# print(content[x].strip())
			# print(content[x+1].strip())
			# print(content[x+2].strip())
			# print("\n\n")




print("total ,"+str(MONTH)+", : "+str(month_count))
print(indexes)


for index in indexes:
	# print(content[index].strip()[0:len(content[index].strip())-1]+"DATE\n")
	content[index] = content[index].strip()[0:len(content[index].strip())-1]+"DATE\n"

out_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually_1.csv"
with open(out_filename, 'w') as out_file:
	out_file.writelines(content)
